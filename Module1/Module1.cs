﻿using System;
using System.Linq;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 mod = new Module1();
             mod.RunMethods();
          
                Console.ReadKey();
        }


        public int[] SwapItems(int a, int b)
        {
            Console.WriteLine("Input values a={0} b={1}", a, b);
            int[] arr = { a, b };
            Array.Reverse(arr);
            Console.WriteLine("Output values a={0} b={1}", arr[0], arr[1]);
            return arr;
        }

        public int GetMinimumValue(int[] input)
        {
            int n = input.Min();
            Console.WriteLine("Minimal value is {0}", n);
            return n;
        }

        public void RunMethods()
        {
            SwapItems(1, 2);
            GetMinimumValue(new int[] {160, 30, 7 });
        }
    }
}
